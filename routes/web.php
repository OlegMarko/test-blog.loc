<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['namespace' => 'Client'], function () {
    Route::get('/', 'HomeController@index');

    Route::get('/get-more-posts', 'HomeController@getMorePosts');

    Route::get('/show-post/{id}', 'HomeController@showPost');

    Route::post('/add-comment/{post_id}', 'CommentController@addComment');
});

Route::resource('post', 'PostController');