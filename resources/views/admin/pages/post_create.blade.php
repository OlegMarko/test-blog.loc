@extends('admin.layouts.app')

@section('content')
    <!-- Page content -->
    <div class="page-content">
        <div class="row">
            <div class="col-md-2">
                <div class="sidebar content-box" style="display: block;">
                    <ul class="nav">
                        <!-- Main menu -->
                        <li class="current"><a href="{{ url('/admin/post') }}"><i
                                        class="glyphicon glyphicon-pencil"></i> Posts</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-10">
                <div class="content-box-large">

                    <div class="content-box-large">
                        <div class="panel-heading">
                            <div class="panel-title">{{ $page_title }}</div>
                        </div>

                        <div class="panel-body">
                            <form data-new-post-form>
                                {{ csrf_field() }}
                                <fieldset>
                                    <div class="col-sm-8 form-group" data-post="image">
                                        <label class="control-label">Картинка</label>
                                        <div>
                                            <input type="file" class="btn btn-default" name="post[image]">
                                            <p class="help-block">
                                                File types: jpg, jpeg, gif, png. Max file size 2Mb
                                            </p>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <img id="img-preview" src="" alt="" style="max-width: 150px; height: auto;">
                                    </div>
                                </fieldset>
                                <fieldset>
                                    <div class="form-group" data-post="title">
                                        <label>Назва</label>
                                        <input class="form-control" placeholder="Назва поста" type="text"
                                               name="post[title]">
                                    </div>

                                    <div class="form-group" data-post="text">
                                        <label>Контент</label>
                                        <textarea name="post[text]" data-post-text></textarea>
                                    </div>
                                </fieldset>

                                <div id="liqpay_checkout"></div>

                                <div data-save-button></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')


<script>
    window.LiqPayCheckoutCallback = function() {
        LiqPayCheckout.init({
            data: "eyJ2ZXJzaW9uIjozLCJhY3Rpb24iOiJwYXkiLCJwdWJsaWNfa2V5IjoiaTU5NjIwODQ4MzI3IiwiYW1vdW50IjoiMTAiLCJjdXJyZW5jeSI6IlVBSCIsImRlc2NyaXB0aW9uIjoicG9zdGVkIiwidHlwZSI6ImJ1eSIsInNhbmRib3giOiIxIiwibGFuZ3VhZ2UiOiJydSJ9",
            signature: "2C2f52effx6vOxOV80Mx5drhk4o=",
            embedTo: "#liqpay_checkout",
            mode: "embed"
        }).on("liqpay.callback", function(data){
            if (data.result == 'success') {
                post.save();
            }
        }).on("liqpay.ready", function(data){
            // ready
        }).on("liqpay.close", function(data){
            // close
        });
    };
</script>
<script src="//static.liqpay.com/libjs/checkout.js" async></script>

<script>
    $(document).ready(function () {
        $('textarea[data-post-text]').summernote({
            height: 300
        });
    });

    $("input[name=\'post[image]\']").change(function () {
        readURL(this);
    });

    /*----------------------------------------------------------------------------*/
    /* Preview image
     /*----------------------------------------------------------------------------*/
    function readURL(input) {

        if (input.files && input.files[0]) {

            var reader = new FileReader();

            reader.readAsDataURL(input.files[0]);

            reader.onload = function (e) {
                $('img#img-preview').attr('src', e.target.result);
            };
        }
    }

    /*----------------------------------------------------------------------------*/
    /* Send form and upload image
     /*----------------------------------------------------------------------------*/
    var post = {
        'save': function () {

            var request = new XMLHttpRequest();
            request.open("POST", "{{ route('post.store') }}");
            request.send(new FormData(document.querySelector("form[data-new-post-form]")));

            request.onload = function () {

                if (request.status == 200) {

                    $('div[data-post]').css({'border': 'none'});

                    alert('Пост додано');

                    return window.location.href = '{{ url('/') }}';
                }

                if (request.status == 422) {

                    var errors = false;

                    if (request.responseText) {
                        errors = JSON.parse(request.responseText);
                    }

                    if (errors) {
                        $('div[data-post]').css({'border': 'none'});

                        if (errors['post.image']) {
                            $('div[data-post=\'image\']').css({'border': '1px solid #c00'});
                        }
                        if (errors['post.title']) {
                            $('div[data-post=\'title\']').css({'border': '1px solid #c00'});
                        }
                        if (errors['post.text']) {
                            $('div[data-post=\'text\']').css({'border': '1px solid #c00'});
                        }

                        return alert('Помилка валідації');
                    }
                }
            };
        }
    }
</script>
@endpush