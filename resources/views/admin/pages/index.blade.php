@extends('admin.layouts.app')

@section('content')
<!-- Page content -->
<div class="page-content">
    <div class="row">
        <div class="col-md-2">
            <div class="sidebar content-box" style="display: block;">
                <ul class="nav">
                    <!-- Main menu -->
                    <li class="current"><a href="{{ route('post.index') }}"><i class="glyphicon glyphicon-paperclip"></i> Posts</a></li>
                    <li class="current"><a href="{{ route('post.create') }}"><i class="glyphicon glyphicon-pencil"></i> New Posts</a></li>
                </ul>
            </div>
        </div>
        <div class="col-md-10">
            <div class="content-box-large">
                <table class="table table-hover">
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Actions</th>
                    </tr>
                    @foreach($posts as $item)
                        <tr>
                            <td>{{ $item->post_id }}</td>
                            <td>{{ $item->name }}</td>
                            <td>
                                <button class="btn btn-danger btn-xs"
                                        onclick="if(confirm('Deleted?')){sendForm('#post-{{ $item->language_id }}');}">
                                    <i class="fa fa-trash"></i></button>
                                <a href="{{ url('/admin/post/'.$item->language_id) }}"
                                   class="btn btn-info btn-xs"><i class="fa fa-pencil"></i></a>

                                <form action="{{ route('post.destroy', $item->language_id) }}" id="post-{{ $item->language_id }}" method="post">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
</div>
<!-- / Page content -->
@endsection