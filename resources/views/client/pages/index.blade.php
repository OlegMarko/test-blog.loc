@extends('client.layouts.app')

@section('content')
    <!-- Main Content -->
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">

                <div data-blog-posts>
                @foreach($posts as $post)
                <div class="post-preview">
                    <a href="{{ url('/show-post/'.$post->post_id) }}">
                        <h2 class="post-title">
                            {{ $post->post_title }}
                        </h2>
                        <h3 class="post-subtitle">
                            {{ str_limit(strip_tags($post->post_text), 25, '...') }}
                        </h3>
                    </a>
                    <p class="post-meta">Posted on {{ $post->created_at }}</p>
                </div>
                <hr>
                @endforeach
                </div>

                <!-- Pager -->
                <ul class="pager">
                    <li class="next">
                        <a href="javascript:void(0);" onclick="loadPost();">Older Posts &rarr;</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- / Main Content -->
@endsection

@push('script')
<script>
    var count = 5;
    var limit = 2;
    var load = false;

    $(document).ready(function() {
        $(window).scroll(function() {
            loadPost();
        });
    });

    function loadPost() {
        if (!load && ($(window).scrollTop() + $(window).height() >= $(document).height()-300)) {

            $.ajax({
                url: "{{ url('/get-more-posts') }}",
                method: 'GET',
                data: {"count": count, "limit": limit},
                beforeSend: function () {
                    load = true;
                }
            }).done(function(response) {

                var basePostURL = "{{ url('/show-post/') }}";

                if (response.posts) {

                    $.each(response.posts, function(index, post) {

                        var description = post['post_text'];
                        if(description.length > 25) description = description.substring(0,25) + '...';

                        $('div[data-blog-posts]').append(
                            '<div class="post-preview">' +
                            '<a href="' + basePostURL + '/' + post['post_id'] + '">' +
                            '<h2 class="post-title">'+
                            post['post_title'] +
                            '</h2>' +
                            '<h3 class="post-subtitle">'+
                            description +
                            '</h3>' +
                            '</a>' +
                            '<p class="post-meta">Posted on ' + post['created_at'] + '</p>' +
                            '</div>' +
                            '<hr>'
                        );
                    });

                    load = false;
                    count += 10;
                }
            });
        }
    }
</script>
@endpush