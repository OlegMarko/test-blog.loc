@extends('client.layouts.app')

@section('content')
    <!-- Post Content -->
    <article>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">

                    {!! $post['post_text'] !!}

                </div>
            </div>

            <div class="row">
                <form action="{{ url('/add-comment/' . $post['post_id']) }}" method="post">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label>Comment</label>
                        <textarea name="comment_text" class="form-control"></textarea>
                    </div>

                    <div class="btn-group">
                        <button type="submit" class="btn btn-success">Add Comment</button>
                    </div>
                </form>

                <hr/>

                @foreach($comments as $comment)
                    <div class="row">
                        <div class="col-md-12 panel-warning">
                            <div class="content-box-header panel-heading">
                                <div class="text-info">{{ $comment->comment_user_name }}</div>
                                <i>{{ $comment->created_at }}</i>
                            </div>
                            <div class="content-box-large box-with-header">
                                {{ $comment->comment_text }}
                                <br /><br />
                            </div>
                        </div>
                    </div>
                @endforeach

            </div>
        </div>
    </article>
    <!-- / Post Content -->

    </form>
@endsection