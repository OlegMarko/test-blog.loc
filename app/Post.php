<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'posts';
    protected $primaryKey = 'post_id';
    protected $fillable = [
        'post_title',
        'post_image',
        'post_text'
    ];

    /**
     * Create new post
     *
     * @param $post_data
     * @return mixed
     */
    protected function addNewPost($post_data)
    {
        return $this->create([
            'post_title' => $post_data['post_title'],
            'post_image' => $post_data['post_image'],
            'post_text' => $post_data['post_text']
        ]);
    }

    /**
     * Return list posts
     *
     * @return mixed
     */
    protected function getAllPost()
    {
        return $this->orderBy('created_at', 'desc')->paginate(10);
    }
}
