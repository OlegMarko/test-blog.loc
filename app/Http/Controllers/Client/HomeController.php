<?php

namespace App\Http\Controllers\Client;

use App\Comment;
use App\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public $data = [];

    /**
     * Show index page (posts)
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $this->data['posts'] = Post::getAllPost();
        $this->data['page'] = [
            'title' => 'Blog',
            'image' => asset('/img/home-bg.jpg')
        ];

        return view('client.pages.index', $this->data);
    }

    public function getMorePosts()
    {
        $count = request('count');
        $limit = request('limit');

        $posts = Post::orderBy('created_at', 'desc')
            ->offset($count)
            ->limit($limit)
            ->get();

        return response()->json(['posts' => $posts]);
    }

    /**
     * Show one post
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showPost($id)
    {
        $this->data['post'] = Post::find($id);

        $this->data['comments'] = Comment::getPostComment($id);

        $this->data['page'] = [
            'title' => $this->data['post']['post_title'],
            'image' => asset('/uploads/'.$this->data['post']['post_image'])
        ];

        return view('client.pages.post', $this->data);
    }
}
