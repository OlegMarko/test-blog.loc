<?php

namespace App\Http\Controllers\Client;

use App\Comment;
use App\Http\Requests\CommentRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CommentController extends Controller
{
    /**
     * Add new comment to post
     *
     * @param CommentRequest $request
     * @param $post_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function addComment(CommentRequest $request, $post_id)
    {
        $data = [
            'comment_user_name' =>'Гість',
            'post_id' => $post_id,
            'comment_text' => $request->get('comment_text')
        ];

        Comment::create($data);

        return redirect()->back()->with('message', 'Коментар успішно доданий');
    }
}
