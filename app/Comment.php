<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = 'comments';
    protected $primaryKey = 'comment_id';
    protected $fillable = [
        'post_id',
        'comment_user_name',
        'comment_text'
    ];

    /**
     * Return post comments
     *
     * @param $post_id
     * @return mixed
     */
    protected function getPostComment($post_id)
    {
        return $this
            ->where('post_id', $post_id)
            ->orderBy('created_at', 'desc')
            ->get();
    }
}
